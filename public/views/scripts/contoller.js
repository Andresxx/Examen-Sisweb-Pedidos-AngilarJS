

const URL="https://sisweb-product-orders.herokuapp.com";
const ORDER="/order";
const PRODUCT_URL="/products";
const ORDERS_BY_STATUS="/orders?status=";
const DRAFT_STATUS="draft";
const REQUESTED_STATUS="requested";
const FINISHED_STATUS="finished";
const USERCI="666";
const productDictionary={};
let currentOrder;


let app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/nuevo", {
        templateUrl : "/views/pedido.html",
        controller : "newCtrl"
    })
    .when("/", {
        templateUrl : "/views/root.html",
        controller : "mainCtrl"
    })
    .when("/verOrden/:id", {
        templateUrl : "/views/pedido.html",
        controller : "seeCtrl"
    })
});



//-----------------------------------   CONTROLADORES --------------------------//
app.controller("seeCtrl",seeCtrl)

function seeCtrl($scope, $http, $routeParams){
    let id=$routeParams.id;
    let req = {
        method: 'GET',
        url: URL+ORDER+"/"+id,
        headers: {
            User:USERCI
        }
    };

    getOrder($http, req, $scope);

    req = {
        method: 'GET',
        url: URL+PRODUCT_URL,
        headers: {
            User:USERCI
        }
    };
    fetchProductToScope($http, req, $scope);
    
    $scope.toIndent = () => {
        currentOrder.status = REQUESTED_STATUS;
        saveOrder($http);
    }

    $scope.save = () => {

        saveOrder($http);
    }

    $scope.addProduct = () => {
        let select = document.querySelector("#table-select");
        let id = select.value;
        $scope.order.products.push(id);
        currentOrder = $scope.order;
        increaseRowCounter();
    }

    $scope.deleteById =(id)=>{
        let aux=[];
        currentOrder.products.forEach(function(x){
            if(x !== id)
            {
                aux.push(x);
            }
        });
        currentOrder.products=[];
        currentOrder.products=aux;
    }
  

}


app.controller("mainCtrl",mainCtrl)

function mainCtrl($scope, $http){

    eventHandlers();

    let req = {
        method: 'GET',
        url: URL+ORDERS_BY_STATUS+DRAFT_STATUS,
        headers: {
          User:USERCI
        }
    }
    fetchDraftOrdersToScope($http,req,$scope);
    req = {
        method: 'GET',
        url: URL+ORDERS_BY_STATUS+REQUESTED_STATUS,
        headers: {
            User:USERCI
        }
    }

    fetchRequestedOrdersToScope($http,req,$scope);
    req = {
        method: 'GET',
        url: URL+ORDERS_BY_STATUS+FINISHED_STATUS,
        headers: {
            User:USERCI          
        }
    }
    fetchFinishedOrdersToScope($http,req,$scope);
}


app.controller("newCtrl",newCtrl);

function newCtrl($scope, $http){
    let req = {
        method: 'GET',
        url: URL+PRODUCT_URL,
        headers: {
            User:USERCI
        }
    };
    fetchProductToScope($http, req, $scope);
    $scope.order={
        "customer": "", 
        "products": [], 
        "status": DRAFT_STATUS
    };

    $scope.toIndent = () => {
        currentOrder.status = REQUESTED_STATUS;
        saveOrder($http);
    }

    $scope.save = () => {

        saveOrder($http);
    }

    $scope.addProduct = () => {
        let select = document.querySelector("#table-select");
        let id=select.value;
        $scope.order.products.push(id);
        currentOrder=$scope.order;
        increaseRowCounter();
    }

    $scope.deleteById =(id)=>{
        let aux=[];
        currentOrder.products.forEach(function(x){
            if(x !== id)
            {
                aux.push(x);
            }
        });
        currentOrder.products=[];
        currentOrder.products=aux;
    }
}

//-----------------------------------  FIN CONTROLADORES --------------------------//



//------------------------------FUNCIONES---------------------------------//
function getOrder(httpService, req, scope){
    httpService(req)
    .then((response) => {
        scope.order = response.data;
        currentOrder = response.data;
    })
    .catch((error) => {
        console.log("Error al recuperar la orden");
    });
}

function simplerMode(){
    let auxiliarOrder=currentOrder;
    currentOrder={
            "customer": "", 
            "products": [], 
            "status": DRAFT_STATUS
        };
    currentOrder.customer=auxiliarOrder.customer;
    currentOrder.products=auxiliarOrder.products;
    currentOrder.status=auxiliarOrder.status;
}

function saveOrder(httpService){
    let req;
    if(currentOrder._id){
        let id=currentOrder._id;
        simplerMode();
        req = {
            method: 'PUT',
            url: URL+ORDER+"/"+id,
            data : JSON.stringify(currentOrder),
            headers: {
                 User:USERCI   
            }
        };
    }
    else{
        req = {
            method: 'POST',
            url: URL+ORDER,
            data : JSON.stringify(currentOrder),
            headers: {
                User:USERCI
            }
        };
    }
    httpService(req)
        .then((response)=>{
            window.location.replace('/views/index.html#!/');
        })
        .catch((error)=>{
            console.log("Error en la redireccion");
        });
}

function fetchDraftOrdersToScope(httpService, req, scope){
    httpService(req)
    .then((response)=>{
        scope.draftOrders=response.data;
    })
    .catch((error)=>{
        console.log("Error al recuperar pedido en borrador para mostrar");

    });
}

function fetchRequestedOrdersToScope(httpService, req, scope){
    httpService(req)
    .then((response)=>{
        scope.requestedOrders=response.data;
    })
    .catch((error)=>{
       console.log("Error al recuperar pedidos soliciatdos para mostrar");
        
    });
}


function fetchFinishedOrdersToScope(httpService, req, scope){
    httpService(req)
    .then((response)=>{
        scope.finishedOrders=response.data;
    })
    .catch((error)=>{
        console.log("Error al recuperar pedido finalizados para mostrar");
    });
}


function chargeInProductDictionary(array){
    for(product of array){
        productDictionary[product._id]=product.name;
    }
}

function fetchProductToScope(httpService, req, scope){
    httpService(req)
    .then((response)=>{
        scope.products=response.data;
        chargeInProductDictionary(scope.products);
        scope.productDictionary=productDictionary;
    })
    .catch((error)=>{
      console.log("Error al cargar la lista productos");
    });
}


//------------------------------  FIN FUNCIONES   ---------------------------------//


